#ifndef DEPLIST_H
#define DEPLIST_H

#include "depnode.h"
#include "filelist.h"

#include <string>

class DepList
{
	public:
		DepList(std::string);
		~DepList();
		void buildDeps(FileList*);
		bool contains(std::string) const;
		std::string getDepString() const;
		std::string getSource() const;
	private:
		void buildFromFile(std::string, FileList*);

		std::string _source;
		DepNode* _head;
		DepNode* _tail;
};

#endif
