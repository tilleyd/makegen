#include "dependencies.h"
#include "deplist.h"
#include "filelist.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// TODO support better structured file structures

int main(int argc, char* argv[])
{
	using namespace std;

	// TODO config & variables in makefile
	string cco("g++ -c");
	string cc("g++");
	string outfile("");

	if (argc == 2)
		outfile = string(argv[1]);
	else
		outfile = string("main");

	// create the dependencies
	FileList fl;
	Dependencies deps(&fl);
	deps.create();

	// create a string of targets
	stringstream stream;
	for (unsigned int i = 0; i < deps.size(); i++) {
		stream << deps.getTarget(i);
		if (i < deps.size() - 1) {
			stream << " ";
		}
	}

	ofstream file("makefile");
	
	if (file.good()) {
		// generate the makefile
		// first the macros
		file << "OFILES = " << stream.str() << endl;
		file << "CC_O = " << cco << endl;
		file << "CC = " << cc << endl;
		file << endl;

		file << outfile << ": $(OFILES)" << endl;
		file << "\t$(CC) $(OFILES) -o " << outfile << endl;
		file << endl;

		for (unsigned int i = 0; i < deps.size(); i++) {
			DepList* list = deps.getList(i);
			file << deps.getTarget(i) << ": " << list->getDepString() << endl;
			file << "\t$(CC_O) " << list->getSource() << endl;
			file << endl;
		}

		file << "run: " << outfile << endl;
		file << "\t./" << outfile << endl << endl;

		file << "clean:" << endl;
		file << "\trm *.o " << outfile << endl;
		file.close();
	} else {
		cerr << "Fatal error: Could not create file 'makefile'" << endl;
	}
}

