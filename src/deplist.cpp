#include "deplist.h"

#include <fstream>

// TODO remove
#include <iostream>

DepList::DepList(std::string filename)
	: _source(filename)
	, _head(NULL)
	, _tail(NULL)
{
	_head = new DepNode(_source);
	_tail = _head;
}

DepList::~DepList()
{
	delete _head;
	_head = NULL;
	_tail = NULL;
}

void DepList::buildDeps(FileList* fl)
{
	buildFromFile(_source, fl);
}

bool DepList::contains(std::string filename) const
{
	DepNode* cur = _head;
	bool found = false;
	while (!found && cur) {
		if (cur->_filename == filename) {
			found = true;
		} else {
			cur = cur->_next;
		}
	}
	return found;
}

std::string DepList::getDepString() const
{
	return _head->getDepString();
}

void DepList::buildFromFile(std::string filename, FileList* fl)
{
	using namespace std;
	// open the source file
	ifstream file(filename.c_str());
	if (file.good()) {
		string line;
		while (getline(file, line)) {
			// search for any '#include "..."' lines
			if (line.find("#include") == 0) {
				int pos = line.find("\"");
				if (pos != std::string::npos) {
					// trim out the filename
					line.erase(0, pos + 1);
					pos = line.find("\"");
					if (pos != std::string::npos) {
						line.erase(pos, line.length() - pos);
					}
					string filename = line;
					
					// check the existence of the dependency
					// TODO using FileList*
					// make sure the dependency isn't already in
					if (!contains(filename)) {
						// create the dependency
						_tail->_next = new DepNode(filename);
						_tail = _tail->_next;
						// search the dependency for dependencies
						buildFromFile(filename, fl);
					}
				}
			}
		}
		file.close();
	}
}

std::string DepList::getSource() const
{
	return _source;
}

