#include "filelist.h"

#include <stdio.h>

FileList::FileList()
	: _file()
{
	using std::string;
	const int BUFSIZE = 2048;
	// read the current directory contents
	FILE* pipe = popen("ls", "r");
	if (pipe) {
		char* buffer = new char[BUFSIZE];
		int len;
		string ls;
		while ((len = fread(buffer, 1, BUFSIZE - 1, pipe)) > 0) {
			// null terminate the string and append to the output
			buffer[len] = '\0';
			ls += buffer;
		}
		ls += "\0";

		separate(ls.c_str(), ls.length());

		// clean up
		pclose(pipe);
		delete [] buffer;
	}
}

std::string FileList::getFile(int i) const
{
	return _file[i];
}

int FileList::size() const
{
	return _file.size();
}

void FileList::separate(const char * c, int size)
{
	std::string temp = "";

	for (int i = 0; i < size; ++i) {
		if (c[i] != '\n' && c[i] != '\0') {
			temp += std::string(1, c[i]);
		}
		else {
			temp += "\0";
			_file.push_back(temp);
			temp = "";
		}
	}
}

