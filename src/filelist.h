#ifndef FILELIST_H
#define FILELIST_H

#include <string>
#include <vector>

class FileList
{
	public:
		FileList();
		std::string getFile(int) const;
		int size() const;
		void print();
	private:
		void separate(const char *, int);			// separate filenames from a string, add each file to _file
		std::vector<std::string> _file;
};

#endif

