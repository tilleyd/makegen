#include "depnode.h"

#include <sstream>

DepNode::DepNode(std::string filename)
	: _filename(filename)
	, _next(NULL)
{}

DepNode::~DepNode()
{
	// TODO here or in DepList?
	if (_next) {
		delete _next;
		_next = NULL;
	}
}

std::string DepNode::getDepString() const
{
	std::stringstream stream;
	stream << _filename;
	if (_next) {
		stream << " " << _next->getDepString();
	}
	return stream.str();
}

