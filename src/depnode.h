#ifndef DEPNODE_H
#define DEPNODE_H

#include <string>

class DepNode
{
	public:
		DepNode(std::string);
		~DepNode();
		std::string getDepString() const;

		DepNode* _next;
		std::string _filename;
};

#endif

