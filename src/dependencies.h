#ifndef DEPENDENCIES_H
#define DEPENDENCIES_H

#include "deplist.h"
#include "filelist.h"

#include <vector>

class Dependencies
{
	public:
		Dependencies(FileList*);
		~Dependencies();
		void create();
		DepList* getList(int) const;
		std::string getTarget(int) const;
		int size() const;
	private:
		FileList* _files;
		std::vector<DepList*> _list;
};

#endif
