#include "dependencies.h"

#include <stdio.h>
#include <string>
#include <iostream>

Dependencies::Dependencies(FileList* f) : _files(f)
{}

Dependencies::~Dependencies()
{}

void Dependencies::create()
{
	for (int i = 0; i < _files->size(); ++i) {
		std::size_t index = _files->getFile(i).find(".cpp");
	
		if (index != std::string::npos) {
			DepList* deplist = new DepList(_files->getFile(i));
			deplist->buildDeps(_files);
			_list.push_back(deplist);
		}
	}


}

DepList* Dependencies::getList(int index) const
{
	return _list[index];
}

int Dependencies::size() const
{
	return _list.size();
}

std::string Dependencies::getTarget(int index) const
{
	DepList* list = getList(index);

	std::string target = "";
	std::string source = list->getSource();

	std::size_t i = source.find(".");
	
	if (i != std::string::npos) {
		target = source.substr(0, i+1);
		target += "o";
	}

	return target;
}
