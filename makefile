# Makefile for project makegen

OFILES = bin/makegen.o bin/filelist.o bin/dependencies.o bin/deplist.o bin/depnode.o
CC_O = g++ -c
CC = g++

makegen: $(OFILES)
	$(CC) $(OFILES) -o makegen

bin/makegen.o: src/makegen.cpp src/filelist.h
	$(CC_O) src/makegen.cpp -o bin/makegen.o

bin/filelist.o: src/filelist.cpp src/filelist.h
	$(CC_O) src/filelist.cpp -o bin/filelist.o

bin/dependencies.o: src/dependencies.cpp src/dependencies.h src/deplist.h src/filelist.h
	$(CC_O) src/dependencies.cpp -o bin/dependencies.o

bin/deplist.o: src/deplist.cpp src/deplist.h
	$(CC_O) src/deplist.cpp -o bin/deplist.o

bin/depnode.o: src/depnode.cpp src/depnode.h
	$(CC_O) src/depnode.cpp -o bin/depnode.o

run: makegen
	./makegen

